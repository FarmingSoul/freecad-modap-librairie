# freeCAD modAP librairie



## Une librairie pour les pièces standards

Cette librairie a pour but d'accompagner les projets d'auto construction agricole .

Elle contiendra les pièces standards agrémentées par les personnes utilisatrices .

Cette librairie contiendra des pièces *.stl *.step *.fCStd

Elle fonctionne actuellement avec le [FreeCAD-library](https://github.com/FreeCAD/FreeCAD-library) sur le logiciel [FreeCAD](https://www.freeCAD.org/)

![Demis-bride](https://wiki.freecadweb.org/images/6/63/PatsLibrary-selector.png)

[VOIR AUSSI](https://wiki.freecadweb.org/Parts_Library_Workbench/fr)